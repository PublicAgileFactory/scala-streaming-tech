---
customTheme: "myTheme"
transition: "concave"
theme: "solarized"
separator: \n---\n
verticalSeparator: \n--\n
# highlightTheme: "dracula"
logoImg: "assets/logo.png"
slideNumber: true
title: "Spark Safe Encoders"
---

# Scala Streaming

--

# Agenda

- Intro
- API
    - spark
    - flink
    - akka
- state
    - spark
    - flink
    - akka
- deployment
    - spark
    - flink
    - akka
- connectors
    - spark
    - flink
    - akka
- Conclusion
  - tabella comparativa e when I should use a,b or c

--

  Intro

--

Intro

---

## Spark

Apache Spark™ is a multi-language engine for executing data engineering, data science, 
and machine learning on single-node machines or clusters.

Spark was born as a batch processing framework, but from very early releases (0.8.0 on Scala 2.9.3) a streaming module was introduced.

--

Spark streaming is a "retro-fit" of the Spark batch engine, applied to very small batches of data continuosly.

![microbatch](assets/spark-streaming-flow.png)

--

Latest version of Spark Streaming technology is based on Spark SQL API and it's called Spark Structured Streaming.

The aim it's to make working with static and streaming data as similar as possible.

--

Consider the input data stream as the "Input Table". Every data item that is arriving on the stream is like a new row being appended to the Input Table.

![streamingtable](assets/structured-streaming-stream-as-a-table.png)

--

Every trigger interval (say, every 1 second), new rows get appended to the Input Table, which update the Result Table. Whenever the result table gets updated, we want to write the result rows to a sink.

<img src="assets/structured-streaming-model.png" style="width:70%;height:70%;"/>

--

The **Output** is defined as what gets written out to the external storage. The output can be defined in different modes:

- Complete Mode
- Append Mode
- Update Mode

--

### API

--

### Or 2 APIs?

--

Spark Streaming exposes 2 different APIs:

- SQL-like API
- Collection-like API

--

#### SQL like

Something that resembles sql

--

#### Collection like

What every user of Scala standard library expects to find:

- `map`, `flatMap`

And something more streaming and spark related:

- `withWatermark`
- `groupByKey`
- `mapPartitions`

--

##### GroupByKey

--

### Stateful

--

### Deployment


--

### Throughput and Latency

---

# That's all folks!

--

# Questions ?